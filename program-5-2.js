const { promisify } = require('util');
const { readFile } = require('fs');
const { join: pathJoin } = require('path');
const read = promisify(readFile);

(async function () {
  const content = await read(pathJoin(__dirname, '/data-5.txt'), 'utf-8');
  const program = content
    .trim()
    .split(',')
    .map(i => Number.parseInt(i));

  function getInstructionAt(currentIndex) {
    const inst = program[currentIndex];
    const opcode = inst % 100;
    const n = opcode === 1 || opcode === 2 || opcode === 7 || opcode === 8 ? 3
      : opcode === 3 || opcode === 4 ? 1
        : opcode === 5 || opcode === 6 ? 2
          : 0;
    const modes = inst.toString()
      .substring(0, inst.toString().length - 2)
      .split('')
      .map(x => Number.parseInt(x))
      .reverse();
    while (modes.length < n) {
      modes.push(0);
    }

    const params = [];

    for (let i = 0; i < n; i += 1) {
      if ([1, 2, 3, 7, 8].includes(opcode) && i === n - 1) {
        params.push(program[currentIndex + i + 1]);
      } else if (modes[i] === 0) {
        params.push(program[program[currentIndex + i + 1]]);
      } else if (modes[i] === 1) {
        params.push(program[currentIndex + i + 1]);
      }
    }

    return {
      opcode,
      params,
      n,
    };
  }

  const output = [];

  for (let i = 0; i < program.length; i += 1) {
    const { opcode, params, n } = getInstructionAt(i);
    switch (opcode) {
      case 1: {
        let left = params[0];
        let right = params[1];
        let position = params[2];
        program[position] = left + right;
        i += n;
        break;
      }
      case 2: {
        let left = params[0];
        let right = params[1];
        let position = params[2];
        program[position] = left * right;
        i += n;
        break;
      }
      case 3: {
        let position = params[0];
        program[position] = 5;
        i += n;
        break;
      }
      case 4: {
        let value = params[0];
        output.push(value);
        i += n;
        break;
      }
      case 5: {
        if (params[0] !== 0) {
          i = params[1] - 1;
        } else {
          i += n;
        }
        break;
      }
      case 6: {
        if (params[0] === 0) {
          i = params[1] - 1;
        } else {
          i += n;
        }
        break;
      }
      case 7: {
        if (params[0] < params[1]) {
          program[params[2]] = 1;
        } else {
          program[params[2]] = 0;
        }
        i += n;
        break;
      }
      case 8: {
        if (params[0] === params[1]) {
          program[params[2]] = 1;
        } else {
          program[params[2]] = 0;
        }
        i += n;
        break;
      }
      case 99: {
        return output;
      }
      default: {
        throw "FAILURE AT " + i + " WITH OPCODE " + opcode;
      }
    }
  }
}())
  .then(console.log)
  .catch(console.error);
