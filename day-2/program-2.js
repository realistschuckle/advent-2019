const { promisify } = require('util');
const { readFile } = require('fs');
const read = promisify(readFile);

function run(program) {
    for (let i = 0; i < program.length; i += 1) {
	const opcode = program[i];
	switch (opcode) {
	case 1: {
	    let left = program[program[i + 1]];
	    let right = program[program[i + 2]];
	    let position = program[i + 3];
	    program[position] = left + right;
	    i += 3;
	    break;
	}
	case 2: {
	    let left = program[program[i + 1]];
	    let right = program[program[i + 2]];
	    let position = program[i + 3];
	    program[position] = left * right;
	    i += 3;
	    break;
	}
	case 99: {
	    return program[0];
	}
	default: {
	    return 0;
	}
	}
    }
}

(async function () {

    const content = await read(__dirname + '/data.txt', 'utf-8');
    const newProgram = () => content
	  .trim()
	  .split(',')
	  .map(i => Number.parseInt(i));

    for (let noun = 0; noun < 100; noun += 1) {
	for (let verb = 0; verb < 100; verb += 1) {

	    const program = newProgram();
	    program[1] = noun;
	    program[2] = verb;

	    if (run(program) === 19690720) {
		return 100 * noun + verb;
	    }

	}
    }
    return 'not found';
    
}())
    .then(console.log)
    .catch(console.error);
