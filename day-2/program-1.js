const { promisify } = require('util');
const { readFile } = require('fs');
const read = promisify(readFile);

(async function () {

    const content = await read(__dirname + '/data.txt', 'utf-8');
    const program = content
	  .trim()
	  .split(',')
	  .map(i => Number.parseInt(i));

    program[1] = 12;
    program[2] = 2;

    for (let i = 0; i < program.length; i += 1) {
	const opcode = program[i];
	switch (opcode) {
	case 1: {
	    let left = program[program[i + 1]];
	    let right = program[program[i + 2]];
	    let position = program[i + 3];
	    program[position] = left + right;
	    i += 3;
	    break;
	}
	case 2: {
	    let left = program[program[i + 1]];
	    let right = program[program[i + 2]];
	    let position = program[i + 3];
	    program[position] = left * right;
	    i += 3;
	    break;
	}
	case 99: {
	    return program[0];
	}
	default: {
	    throw "FAILURE AT " + i + " WITH OPCODE " + opcode;
        }
	}
    }
    
}())
    .then(console.log)
    .catch(console.error);
