const { promisify } = require('util');
const fs = require('fs');

(async function() {
    const readFile = promisify(fs.readFile)
    const content = await readFile(__dirname + '/data.txt', 'utf-8');
    const inputs = content.split('\n').map(i => Number.parseInt(i)).filter(i => !isNaN(i));
    let total = 0;

    for (let input of inputs) {
	total += Math.floor(input / 3) - 2;
    }

    console.log('processed', inputs.length, 'numbers');

    return total;
}())
    .then(console.log)
    .catch(console.error);

