const { promisify } = require('util');
const fs = require('fs');

(async function() {
    function calcFuel(input) {
	return Math.max(0, Math.floor(input / 3) - 2);
    }

    function calcTotalFuel(input) {
	let total = 0;
	let neededFuel = calcFuel(input);
	do {
	    total += neededFuel;
	} while (neededFuel = calcFuel(neededFuel));
	return total;
    }

    const readFile = promisify(fs.readFile)
    const content = await readFile(__dirname + '/../data.txt', 'utf-8');
    return content.split('\n')
	.map(i => Number.parseInt(i))
	.filter(i => !isNaN(i))
	.map(i => calcTotalFuel(i))
	.reduce((a, i) => a + i, 0);
}())
    .then(console.log)
    .catch(console.error);

