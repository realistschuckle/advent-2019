const { promisify } = require('util');
const { readFile } = require('fs');
const { join: pathJoin } = require('path');
const read = promisify(readFile);

(async function () {
  const content = await read(pathJoin(__dirname, 'data-3.txt'), 'utf-8');
  const program = content
    .trim()
    .split('\n')
    .map(wire => wire.split(','))
    .map(wire => wire.map(inst => ([inst[0], Number.parseInt(inst.substring(1))])));

  const breadBoard = [];
  const initX = 5000;
  const initY = 5000;
  for (let wireIndex = 0; wireIndex < program.length; wireIndex += 1) {
    let x = initX;
    let y = initY;
    const wire = program[wireIndex];
    for (let instIndex = 0; instIndex < wire.length; instIndex += 1) {
      const inst = wire[instIndex];
      for (let i = 0; i < inst[1]; i += 1) {
        switch (inst[0]) {
          case 'U': {
            y += 1;
            break;
          }
          case 'D': {
            y -= 1;
            break;
          }
          case 'L': {
            x -= 1;
            break;
          }
          case 'R': {
            x += 1;
            break;
          }
        }
        if (!breadBoard[x]) {
          breadBoard[x] = [];
        }
        if (!breadBoard[x][y]) {
          breadBoard[x][y] = [];
        }
        if (breadBoard[x][y].indexOf(wireIndex) < 0) {
          breadBoard[x][y].push(wireIndex);
        }
      }
    }
  }

  let min = Number.MAX_VALUE;
  for (let i = 0; i < breadBoard.length; i += 1) {
    if (!breadBoard[i]) continue;
    for (let j = 0; j < breadBoard[i].length; j += 1) {
      const entries = breadBoard[i][j] || [];
      if (entries.length > 1) {
        min = Math.min(Math.abs(i - initX) + Math.abs(j - initY), min);
      }
    }
  }

  return min;
}())
  .then(console.log)
  .catch(console.error);
