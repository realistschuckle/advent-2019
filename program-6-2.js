const { promisify } = require('util');
const { readFile } = require('fs');
const { join: pathJoin } = require('path');
const read = promisify(readFile);

(async function () {
  const content = await read(pathJoin(__dirname, '/data-6.txt'), 'utf-8');
  const program = content
    .trim()
    .split('\n')
    .map(orbit => orbit.split(")"));

  const orbits = new Map();
  for (let [center, orbiter] of program) {
    orbits.set(orbiter, center);
  }

  function buildPath(value) {
    const path = [];
    while (value !== undefined) {
      value = orbits.get(value);
      path.push(value);
    }
    return path;
  }

  const myPath = buildPath("YOU");
  const sanPath = buildPath("SAN");
  myPath.reverse();
  sanPath.reverse();

  let intersection = undefined;
  for (let i = myPath.length - 1; i >= 0; i -= 1) {
    if (sanPath.includes(myPath[i])) {
      intersection = myPath[i];
      break;
    }
  }

  const myIndex = myPath.lastIndexOf(intersection);
  const sanIndex = sanPath.lastIndexOf(intersection);
  const total = myPath.length - myIndex + sanPath.length - sanIndex - 2;

  return total;

}())
  .then(console.log)
  .catch(console.error);
