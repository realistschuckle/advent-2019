const { promisify } = require('util');
const { readFile } = require('fs');
const read = promisify(readFile);

//        432109876543210987654321
// index: xyaaaaaaaaaaabbbbbbbbbbb

function generateIndex(x, y) {
    const xSign = x < 0 ? 1 << 23 : 0;
    const ySign = y < 0 ? 1 << 22 : 0;
    const a = Math.abs(x) << 11;
    const b = Math.abs(y);
    return xSign + ySign + a + b;
}

function unpackIndex(coord) {
    const xSign = coord >> 23 ? -1 : 1;
    const ySign = (coord >> 22) & 1 ? -1 : 1;
    const a = (coord >> 11) & 2047;
    const b = coord & 2047;
    return [xSign * a, ySign * b];
}

(async function () {

    const content = await read(__dirname + '/data.txt', 'utf-8');
    const program = content
	  .trim()
	  .split('\n')
	  .map(wire => wire.split(','))
	  .map(wire => wire.map(inst => [inst[0], Number.parseInt(inst.substring(1))]));

    const breadBoard = new Map();
    for (let wire = 0; wire < program.length; wire += 1) {
	let x = 1000;
	let y = 1000;
	for (let inst = 0; inst < program[wire].length; inst += 1) {
	    const [direction, distance] = program[wire][inst];
	    for (let delta = 0; delta < distance; delta += 1) {
		switch (direction) {
		case 'U': {
		    y += 1;
		    break;
		}
		case 'D': {
		    y -= 1;
		    break;
		}
		case 'L': {
		    x -= 1;
		    break;
		}
		case 'R': {
		    x += 1;
		    break;
		}
		}
		const coord = generateIndex(x, y);
		if (!breadBoard.has(coord)) {
		    breadBoard.set(coord, new Set());
		}
		breadBoard.get(coord).add(wire);
	    }
	}
    }

    return Array.from(breadBoard.entries())
	.map(([key, value]) => value.size > 1 ? key : null)
	.filter(key => key)
	.map(unpackIndex)
	.reduce((distance, [x, y]) => {
	    return Math.min(distance, Math.abs(x) + Math.abs(y))
	}, Number.MAX_VALUE);

}())
    .then(console.log)
    .catch(console.error);

