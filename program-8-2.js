const { promisify } = require('util');
const { readFile } = require('fs');
const { join: pathJoin } = require('path');
const read = promisify(readFile);

(async function () {
  const content = await read(pathJoin(__dirname, 'data-8.txt'), 'utf-8');
  const program = content.trim();

  const layers = [];
  for (let i = 0; i < program.length; i += 25 * 6) {
    const layer = [];
    for (let j = i; j < i + 25 * 6; j += 25) {
      layer.push(program.substr(j, 25));
    }
    layers.push(layer);
  }

  const image = [[], [], [], [], [], []];
  for (let y = 0; y < 6; y += 1) {
    for (let x = 0; x < 25; x += 1) {
      for (let l = 0; l < layers.length; l += 1) {
        if (layers[l][y][x] === '2') {
          continue;
        }
        image[y][x] = layers[l][y][x];
        break;
      }
    }
  }

  return image.map(row => row.join('').replace(/0/g, ' ')).join('\n');
}())
  .then(console.log)
  .catch(console.error);
