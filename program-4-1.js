(async function () {
  const lower = 234208;
  const upper = 765869;

  function hasDouble(n) {
    const digits = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    for (let c of n.toString()) {
      digits[c] += 1;
      if (digits[c] > 1) {
        return true;
      }
    }
    return false;
  }

  function neverDecreases(n) {
    const s = n.toString();
    for (let i = 0; i < s.length - 1; i += 1) {
      if (s[i] > s[i + 1]) {
        return false;
      }
    }
    return true;
  }

  let total = 0;
  for (let password = lower; password <= upper; password += 1) {
    if (hasDouble(password) && neverDecreases(password)) {
      total += 1;
    }
  }

  return total;
}())
  .then(console.log)
  .catch(console.error);
