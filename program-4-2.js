(async function () {
  const lower = 234208;
  const upper = 765869;

  function hasDouble(n) {
    const s = n.toString();
    for (let i = 0; i < s.length - 1; i += 1) {
      if (i <= s.length - 3 && s[i] === s[i + 1] && s[i] === s[i + 2]) {
        const value = s[i];
        while (i < s.length - 1 && value === s[i + 1]) {
          i += 1;
        }
      } else if (s[i] === s[i + 1] && (i === s.length - 2 || s[i] !== s[i + 2])) {
        return true;
      }
    }
    return false;
  }

  function neverDecreases(n) {
    const s = n.toString();
    for (let i = 0; i < s.length - 1; i += 1) {
      if (s[i] > s[i + 1]) {
        return false;
      }
    }
    return true;
  }

  let total = 0;
  const passwords = [];
  for (let password = lower; password <= upper; password += 1) {
    if (hasDouble(password) && neverDecreases(password)) {
      passwords.push(password);
      total += 1;
    }
  }

  console.log(passwords);
  return total;
}())
  .then(console.log)
  .catch(console.error);
